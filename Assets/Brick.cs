﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Brick : MonoBehaviour {

    public Text countText;
    public int count;

	 void OnCollisionEnter2D(Collision2D collision)
	{
        Destroy(gameObject);
        count += 1;
	}

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";
        }
    }
}
